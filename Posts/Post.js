class Post {
    constructor(image, video, text) {
        this.image = image;
        this.video = video;
        this.text = text;
        this.likes = new Set();
        this.dislikes = new Set();
    }

    like(user){
       this.likes.add(user);
       if(this.dislikes.has(user)){
           this.dislikes.delete(user)
       }
    }
    dislike(user){
        this.dislikes.add(user)
        if (this.likes.has(user)){
            this.likes.delete(user)
        }
    }

    likesCount(){
     return  Array.from(this.likes.values).length
    }

    dislikesCount(){
     return  Array.from(this.dislikes.values).length
    }
    
    

    

}




