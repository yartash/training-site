"use strict";
exports.__esModule = true;
var jwt = require("jsonwebtoken");
var whole = {
    number: 123,
    str: 'string',
    bool: false,
    age: null,
    unknown: undefined,
    id: Symbol,
    user: {
        name: 'valod'
    }
};
function jsonParser(obj) {
    return JSON.stringify(obj);
}
var jsonParsing = jsonParser(whole);
console.log(jsonParsing);
function tok(inp) {
    return jwt.sign(inp, 'shhhhh');
}
var token = tok(jsonParsing);
console.log(token);
function decoded(incomingArg) {
    // @ts-ignore
    return jwt.verify(incomingArg, 'shhhhh');
}
var decoding = decoded(token);
console.log(decoding);
