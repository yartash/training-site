import * as jwt from 'jsonwebtoken';

let whole = {
    number: 123,
    str: 'string',
    bool: false,
    age: null,
    unknown: undefined,
    id: Symbol,
    user: {
        name: 'valod'
    }
};
function jsonParser (obj: {}):string {
    return JSON.stringify(obj);
}
let jsonParsing = jsonParser(whole);
console.log(jsonParsing);


function tok(inp: string):string {
   return jwt.sign(inp, 'shhhhh');
}
let token = tok(jsonParsing)
console.log(token)


function decoded (incomingArg:string):object{
    // @ts-ignore
    return jwt.verify(incomingArg, 'shhhhh');
}
const decoding = decoded(token)
console.log(decoding)
