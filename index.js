"use strict";
exports.__esModule = true;
var express = require("express");
var app = express();
app.get("/", function (req, res) {
    res.send("Hello World");
});
var PORT = process.env.PORT || 3000;
app.listen(PORT, function () {
console.log("Server is running in http://localhost:" + PORT);
});

// const express = require('express');
// const url = require("url");
// const app = express();
// const port = 3000;
//
// // respond with "hello world" when a GET request is made to the homepage
// app.get('/', function(req, res) {
//     res.send('hello world');
//     console.log(req.method);
//     let urlRequest = url.parse(req.url, true);
//     console.log(urlRequest)
//     console.log(urlRequest.query.test);
// });
//
// app.listen(port, () => {
//     console.log(`Server is running in http://localhost:${port}`)
// })