"use strict";
exports.__esModule = true;
// @ts-ignore
var lodash_1 = require("lodash");
var incomeArg = {
    Name: "sam",
    surName: "zaqaryan",
    aGe: 21,
    hasWife: "not yet"
};
var Change = /** @class */ (function () {
    function Change() {
    }
    Change.prototype.serialize = function (obj) {
        // @ts-ignore
        return Object.fromEntries(Object.entries(obj)
            .map(function (_a) {
            var key = _a[0], value = _a[1];
            var newKey = lodash_1["default"].snakeCase(key);
            return [newKey, value];
        }));
    };
    Change.prototype.deserialize = function (obj) {
        // @ts-ignore
        return Object.fromEntries(Object.entries(obj)
            .map(function (_a) {
            var key = _a[0], value = _a[1];
            var newKey = lodash_1["default"].camelCase(key);
            return [newKey, value];
        }));
    };
    return Change;
}());
var ok = new Change();
// @ts-ignore
console.log(ok.serialize(incomeArg));
// @ts-ignore
console.log(ok.deserialize);
