// @ts-ignore
import _ from 'lodash';

const incomeArg = {
    Name: "sam",
    surName: "zaqaryan",
    aGe: 21,
    hasWife: "not yet"
}

class Change {
    serialize(obj:{}): {}{
        // @ts-ignore
        return Object.fromEntries(
            Object.entries(obj)
                .map(([key, value]) => {
                    const newKey = _.snakeCase(key)
                    return [newKey, value]
                })
        );
    }

    deserialize(obj:{}) {
        // @ts-ignore
        return Object.fromEntries(
            Object.entries(obj)
                .map(([key, value]) => {
                    const newKey = _.camelCase(key)
                    return [newKey, value]
                })
        );
    }

}



const ok = new Change();


// @ts-ignore
console.log(ok.serialize(incomeArg));

// @ts-ignore
console.log(ok.deserialize);





