import mongoose from "mongoose"
const { Schema } = mongoose;


const Comments = new Schema ({
    _id: {type:Schema.Types.ObjectId, auto: true},
    author: {type: Schema.Types.ObjectId},
    postId: {type: Schema.Types.ObjectId},
    text: {type: String},
    likes: {type: Number},
    parent: {type: Schema.Types.ObjectId}
})
