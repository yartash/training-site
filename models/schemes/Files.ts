import mongoose from 'mongoose';
const { Schema } = mongoose;

const Files = new Schema ({
    _id: {type: Schema.Types.ObjectId, auto: true},
    author: {type: Schema.Types.ObjectId},
    url: {type: String},
    type: {type: String},
})