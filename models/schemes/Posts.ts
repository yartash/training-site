import mongoose from 'mongoose';
const { Schema } = mongoose;

const Posts = new Schema ({
    _id: {type:Schema.Types.ObjectId, auto: true},
    author: {type: Schema.Types.ObjectId},
    text: {type: String},
    likes: {type: Number},
    comments: {type: Number},
    images: [{ type : Schema.Types.ObjectId}],
    videos: [{ type : Schema.Types.ObjectId}],
    origin: {type: Schema.Types.ObjectId},
    parent: {type: Schema.Types.ObjectId},

})