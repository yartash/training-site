import mongoose from 'mongoose';
const { Schema } = mongoose;

const Relations = new Schema ({
    _id: {type:Schema.Types.ObjectId, auto: true},
    source: {type: Schema.Types.ObjectId},
    target: {type: Schema.Types.ObjectId},
    type: {type: String},
    date: {type: Date.now()}
})


