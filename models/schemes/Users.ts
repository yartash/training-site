import mongoose from 'mongoose';
const { Schema } = mongoose;

const User = new Schema ({
    _id: {type:Schema.Types.ObjectId, auto: true},
    name: {type: String, required: true, unique: true},
    lastName: {type: String, required: true},
    email: {type: String, required: true},
    age: {type: Number}
})

