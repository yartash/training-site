import mongoose from 'mongoose';
const { Schema } = mongoose;

const Relations = new Schema ({

    _id: {type : ObjectId},
    source: {type : ObjectId},
    target: {type : ObjectId},
    type: {type : String},
    date: {type : Date}
    
})

