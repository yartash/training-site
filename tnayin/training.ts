import _ from 'lodash';
import { Schema, model, Model, Document, isValidObjectId } from 'mongoose'
class SDS {
    serialize<M, D>(obj: M): D {
        return Object.fromEntries(
            Object.entries(obj)
            .map(([key, value]) => {
                const newKey = _.snakeCase(key);
                switch (true) {
                    case Object.prototype.toString.call(value) === '[object Object]':
                        return [newKey, this.serialize(value)]
                    default:
                        return [newKey, value];
                }
            }),
        ) as D;
    }
    deSerialize<D, M>(object: D): M {
        return Object.fromEntries(
            Object.entries(object)
            .map(([key, value]) => {
                const newKey = _.camelCase(key)
                switch (true) {
                    case Object.prototype.toString.call(value) === '[object Object]':
                        return [newKey, this.deSerialize(value)]
                    default:
                        return [newKey, value];
                }
            }),
        ) as M;
    }
}


